<?php
if ( ! function_exists('avada_child_theme_enqueue_styles')) {
  function avada_child_theme_enqueue_styles() {
    //Load Styles
    $parent_style = 'avada-style'; // This is 'twentysixteen-style' for the Twenty Sixteen theme.
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' ); // load parent template stylesheet

    // wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,700,300italic,700italic', false ); // preffered way of loading Google fonts

    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', wp_get_theme()->get('Version') ); // load child theme's stylesheet
    // wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/dist/style.css', array( $parent_style, 'wpb-google-fonts' ), wp_get_theme()->get('Version') ); // load child theme's stylesheet

    // Load JS
    wp_register_script('child-js', get_stylesheet_directory_uri() . '/dist/bundle.js', array('jquery'), false, true );
    wp_enqueue_script('child-js');
  }
  add_action( 'wp_enqueue_scripts', 'avada_child_theme_enqueue_styles' );
}
