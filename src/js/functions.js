// uses ES6

jQuery.noConflict();
var Functions = (($, w) => { //shortcuts $=jQuery, w=window
  let _ = (...myVar) => console.log(...myVar);
  var APP = {
    init: () => {
      //start methods within APP
      _(`Avada:child's Theme - JS functions initiated`);
    }
  };
  $(document).ready(() => {
    APP.init();
  });
})(jQuery, window, undefined);

module.exports = Functions;